# SportTrack

![GitLab (self-managed)](https://img.shields.io/gitlab/license/101558?gitlab_url=https%3A%2F%2Fframagit.org)


![](assets/homepage.png)

Par Jean-Louis Emeraud et Benjamin Picard - Groupe INFO1A

## Technologies utilisés 

La majorité du design du site a été réalisé grace a Bootstrap 5, dont le CSS est importé dans chaque page. Cela facilité la disposition des éléments ainsi que le design graphique des éléments ainsi que l'aspect responsive du site. Pour le déploiement facilité, les outils GNU Make et Docker ont été utilisés afin de pouvoir visualiser le site après changement facilement

Merci à eux

## Tests

Le site à été testé sur Firefox sur Windows et Linux, Opera sur Linux, Chrome sur Windows, Firefox sur Android. Des test sont prévus sur le navigateur de la Nintendo 3DS. 

Aucun JavaScript n'est présent, donc la page devrait fonctionner sur un navigateur ne supportant pas le JavaScript "moderne"

## Captures d'écran

![](assets/homepage.png)

Page d'accueil

![](assets/dashboard.png)

Tableau de bord

![Page d'inscription](assets/signup.png)


## Installation

SportTrack est installable sur un serveur de 3 façons possibles : 

### Installation manuelle 

Copiez le contenu du dossier `public-html` dans le répertoire servi par votre serveur

### Installation simplifiée

Exécutez `make install`. Cela installera les fichiers de `public-html` dans `/var/www/html`.

### Installation Docker

Une Dockerfile est inclue dans le dépot. Une tâche `make` est également disponible pour compiler et lancer le conteneur. Commencez par exécuter `make docker` pour compiler l'image et `make docker-run` pour lancer l'image dans un conteneur. Le site est disponible sur le port 8080. Il est possible de regénérer une image Docker après modification avec `make restart`. Cette méthode est plus utile lors du développement. L'image docker est basée sur `httpd:alpine`. L'image est étiqueté `sporttrack:latest`.

**Attention** : Cette méthode recrée une image a chaque exécution (environ 50Mo). N'oubliez pas de retirer les images obsolètes

## Connection

Si vous avez installé SportTrack sur votre serveur, l'application sera accessible depuis l'adresse `localhost:<port>`. Le port est généralement 80. Dans ce cas, il n'est pas nécessaire de l'indiquer. Pour Docker, le conteneur est accessible sur le port 8080. Il est possible de modifier le port d'écoute en modifiant le Makefile.

### Instance publique

Une instance est accessible sur [https://xotak.frama.io/sporttrack](https://xotak.frama.io/sporttrack). Elle est déployée grace a une pipeline déclarée dans le fichier `.gitlab-ci.yml`