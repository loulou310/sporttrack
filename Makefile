docker:
	sudo docker build -t sporttrack .

docker-run:
	sudo docker run -dit --name sporttrack -p 8080:80 sporttrac

docker-stop:
	sudo docker rm -f sporttrack

restart:
	sudo docker rm -f sporttrack
	sudo docker build -t sporttrack .
	sudo docker run -dit --name sporttrack -p 8080:80 sporttrack

install:
	sudo cp ./public-html/* /var/www/html/